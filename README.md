# Richa_QA_Test

Updated Test run for checking REST API response code# Test

## installation steps:
Installed jdk 1.8 and set the environmental variables
installed Eclipse IDE
installed Build automation tool Maven and set the environmental variables
Added Serenity,cucumber and restassured dependencies in the pom.xml


## API test cases:
validating the Spotify Rest API to get the desired album for the id and marketcode.
Used Cucumber gherkin Syntax to write the test cases
Created a feature file under src/test/resources (Spotify1.feature)
Generated the glue code and created a stepdefinition file under src/test/java (SpotifyAlbum.java)
Test is being run through Serenity with Cucumber BDD framework


## How to run test:
Run the Serenity Test runner with Junit 

## How to generate report
Open the terminal
Navigate to the directory where the folder is present
type mvn clean verify 
Serenity Report folder: target/site/serenity/index.html



### References:
https://github.com/serenity-bdd
https://serenity-bdd.github.io/theserenitybook/latest/index.html


Thank you
Richa Ahuja




